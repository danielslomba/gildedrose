This project tries to solve the GildedRose kata presented here: https://github.com/emilybache/GildedRose-Refactoring-Kata

Final implementation uses the Strategy Pattern.

To run the tests:

- `mvn clean test` or 
- run the tests from your IDEA

To see jacoco code coverage report:

- `target/site/jacoco/index.html` or
- It is also published on gitlab: go to pipelines -> click on the job test -> click browse on the Job Artifacts -> go to target/site/jacoco/index.html

GitLab CI/CD is enabled, only to perform the integration tests and the checkstyle.