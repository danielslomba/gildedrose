package com.gildedrose;

import com.gildedrose.update.ItemUpdater;

class GildedRose {
    final Item[] items;

    final ItemUpdater itemUpdater = new ItemUpdater();

    GildedRose(final Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (final Item item : items) {
            itemUpdater.updateQuality(item);
        }
    }
}
